from django.conf.urls import include, url
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('stats', views.StatsView)
router.register('lukaszpilatowski', views.StatsLPView)
router.register('robertolejnik', views.StatsROView)
router.register('martakoziel', views.StatsMKView)
router.register('jacekchmielewski', views.StatsJCView)
router.register('kamilchudy', views.StatsKCView)
router.register('andrzejpiasecki', views.StatsAPView)
router.register('krystiankur', views.StatsKKView)
router.register('paulinamaludy', views.StatsPMView)
router.register('michalgryczka', views.StatsMGView)
router.register('antekmilkowski', views.StatsAMView)
router.register('januszgadek', views.StatsJGView)
router.register('jaroslawmarciniak', views.StatsJMView)
router.register('krzysztofkrzysztofik', views.StatsKKRView)
router.register('authors', views.AuthorsView)

urlpatterns = [
    url(r'^', include(router.urls)),
]


