from django.shortcuts import render
from rest_framework import viewsets
from .models import Authors, Stats, StatsLP, StatsRO, StatsMK, StatsJC, StatsKC, StatsAP, StatsKK, StatsPM, StatsMG, StatsAM, StatsJG, StatsJM, StatsKKR
from .serializers import StatsSerializer, AuthorsSerializer, StatsLPSerializer, StatsROSerializer, StatsMKSerializer, StatsJCSerializer, StatsKCSerializer, StatsAPSerializer, StatsKKSerializer, StatsPMSerializer, StatsMGSerializer, StatsAMSerializer, StatsJGSerializer, StatsJMSerializer, StatsKKRSerializer

class AuthorsView(viewsets.ModelViewSet):
    queryset = Authors.objects.all()
    serializer_class = AuthorsSerializer

class StatsView(viewsets.ModelViewSet):
    queryset = Stats.objects.all()
    serializer_class = StatsSerializer


class StatsLPView(viewsets.ModelViewSet):
    queryset = StatsLP.objects.all()
    serializer_class = StatsLPSerializer


class StatsROView(viewsets.ModelViewSet):
    queryset = StatsRO.objects.all()
    serializer_class = StatsROSerializer


class StatsMKView(viewsets.ModelViewSet):
    queryset = StatsMK.objects.all()
    serializer_class = StatsMKSerializer


class StatsJCView(viewsets.ModelViewSet):
    queryset = StatsJC.objects.all()
    serializer_class = StatsJCSerializer


class StatsKCView(viewsets.ModelViewSet):
    queryset = StatsKC.objects.all()
    serializer_class = StatsKCSerializer


class StatsAPView(viewsets.ModelViewSet):
    queryset = StatsAP.objects.all()
    serializer_class = StatsAPSerializer


class StatsKKView(viewsets.ModelViewSet):
    queryset = StatsKK.objects.all()
    serializer_class = StatsKKSerializer


class StatsPMView(viewsets.ModelViewSet):
    queryset = StatsPM.objects.all()
    serializer_class = StatsPMSerializer


class StatsMGView(viewsets.ModelViewSet):
    queryset = StatsMG.objects.all()
    serializer_class = StatsMGSerializer


class StatsAMView(viewsets.ModelViewSet):
    queryset = StatsAM.objects.all()
    serializer_class = StatsAMSerializer


class StatsJGView(viewsets.ModelViewSet):
    queryset = StatsJG.objects.all()
    serializer_class = StatsJGSerializer


class StatsJMView(viewsets.ModelViewSet):
    queryset = StatsJM.objects.all()
    serializer_class = StatsJMSerializer


class StatsKKRView(viewsets.ModelViewSet):
    queryset = StatsKKR.objects.all()
    serializer_class = StatsKKRSerializer
