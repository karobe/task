from django.db import models

class Stats(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats'


class StatsLP(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_łukasz_piłatowski'

class StatsRO(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_robert_olejnik'

class StatsMK(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_marta_kozieł'

class StatsJC(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_jacek_chmielewski'


class StatsKC(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_kamil_chudy'


class StatsAP(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_andrzej_piasecki'


class StatsKK(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_krystian_kur'


class StatsPM(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_paulina_maludy'


class StatsMG(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_michał_gryczka'


class StatsAM(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_antek_miłkowski'

class StatsJG(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_janusz_gądek'


class StatsJM(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_jarosław_marciniak'


class StatsKKR(models.Model):
#    id = models.IntegerField(primary_key=True)
    word = models.TextField()
    counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stats_krzysztof_krzysztofik'

class Authors(models.Model):
#    id = models.IntegerField(primary_key=True)
    address = models.TextField()
    name = models.TextField()

    class Meta:
        managed = False
        db_table = 'authors'

