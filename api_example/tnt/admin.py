from django.contrib import admin
from .models import Authors, Stats, StatsLP, StatsRO, StatsMK, StatsJC, StatsKC, StatsAP, StatsKK, StatsPM, StatsMG, StatsAM, StatsJG, StatsJM, StatsKKR

admin.site.register(Stats)
admin.site.register(StatsLP)
admin.site.register(StatsRO)
admin.site.register(StatsMK)
admin.site.register(StatsJC)
admin.site.register(StatsKC)
admin.site.register(StatsAP)
admin.site.register(StatsKK)
admin.site.register(StatsPM)
admin.site.register(StatsMG)
admin.site.register(StatsAM)
admin.site.register(StatsJG)
admin.site.register(StatsJM)
admin.site.register(StatsKKR)
admin.site.register(Authors)
# Register your models here.
