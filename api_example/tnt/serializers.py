from rest_framework import serializers
from .models import Authors, Stats, StatsLP, StatsRO, StatsMK, StatsJC, StatsKC, StatsAP, StatsKK, StatsPM, StatsMG, StatsAM, StatsJG, StatsJM, StatsKKR

class AuthorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Authors
        fields = ('address', 'name')

class StatsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stats
        fields = ('word', 'counter')

class StatsLPSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsLP
        fields = ('word', 'counter')

class StatsROSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsRO
        fields = ('word', 'counter')

class StatsMKSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsMK
        fields = ('word', 'counter')

class StatsJCSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsJC
        fields = ('word', 'counter')

class StatsKCSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsKC
        fields = ('word', 'counter')

class StatsAPSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsAP
        fields = ('word', 'counter')

class StatsKKSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsKK
        fields = ('word', 'counter')

class StatsPMSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsPM
        fields = ('word', 'counter')

class StatsMGSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsMG
        fields = ('word', 'counter')

class StatsAMSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsAM
        fields = ('word', 'counter')

class StatsJGSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsJG
        fields = ('word', 'counter')

class StatsJMSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsJM
        fields = ('word', 'counter')

class StatsKKRSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsKKR
        fields = ('word', 'counter')


