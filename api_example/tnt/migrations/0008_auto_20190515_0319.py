# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-05-15 03:19
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tnt', '0007_authors_stats_statsam_statsap_statsjc_statsjg_statsjm_statskc_statskk_statskkr_statslp_statsmg_stats'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='statsam',
            table='stats_antek_milkowski',
        ),
        migrations.AlterModelTable(
            name='statsjg',
            table='stats_janusz_gadek',
        ),
        migrations.AlterModelTable(
            name='statsjm',
            table='stats_jaroslaw_marciniak',
        ),
        migrations.AlterModelTable(
            name='statslp',
            table='stats_lukasz_pilatowski',
        ),
        migrations.AlterModelTable(
            name='statsmg',
            table='stats_michal_gryczka',
        ),
        migrations.AlterModelTable(
            name='statsmk',
            table='stats_marta_koziel',
        ),
    ]
