# Generated by Django 2.2.1 on 2019-05-08 18:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tnt', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='words',
            name='number',
        ),
        migrations.RemoveField(
            model_name='words',
            name='word',
        ),
    ]
