FROM python:3.6

# USER app
ENV PYTHONUNBUFFERED 1
# RUN mkdir /db
#RUN chown app:app -R /db

RUN mkdir /code
ADD . /code
WORKDIR /code
#ADD requirements.txt /code/
RUN pip install -r requirements.txt

RUN python -m nltk.downloader punkt
RUN python -m nltk.downloader stopwords
CMD ["python", "app.py"]
COPY ./run.sh /
ENTRYPOINT ["/run.sh"]
