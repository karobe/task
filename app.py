import psycopg2
from bs4 import BeautifulSoup
import grequests
import requests

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

from collections import Counter

def get_data(website):
    """Returns generator objects from posts and authors from web scraping.

    Parameters
    ----------
    website : str
        address of the website
    """
    addresses = []

    #Extracting number of pages on website
    source = requests.get(website).text
    soup = BeautifulSoup(source, 'lxml')
    pages = int(soup.find('span', class_ = 'page-number').text[-1])

    #Extracting addresses of articles on website
    for page in range(1, pages + 1):
        if page == 1:
            website_page = website
        else:
            website_page = website + '/page/' + str(page)

        source = requests.get(website_page).text
        soup = BeautifulSoup(source, 'lxml')

        for article in soup.find_all('div', class_ = 'post-content'):
            address = website + article.a['href'].split('blog')[-1]
            addresses.append(address)

    reqs = [grequests.get(address) for address in addresses]
    resp = grequests.map(reqs)

    #Extracting posts and post author names
    for r in resp:
        r.encoding
        r.text
        r.encoding
        r.encoding = 'utf-8'
        soup = BeautifulSoup(r.text, 'lxml')

        article_content = soup.find('div', class_ = 'post-content').text
        article_author = soup.find('span', class_ = 'author-name').text
        yield resp.index(r), article_author, article_content

def extract_words(records):
    """Returns Counter object of most frequent words within text."""
    
    all_words = []
    #List for words, with exclusion of punctuation
    words_no_punct = []

    for r in records:
        paragraph = r[2].strip().split('\n')
        for p in paragraph:
            for word in p.split():
                #Excluding words containing addresses
                #and splitting the rest
                if 'com' not in word:
                    if '/' in word:
                        elements = word.split('/')
                        for e in range(len(elements)):
                            all_words.append(elements[e].lower())
                    elif '-' in word:
                        elements = word.split('-')
                        for e in range(len(elements)):
                            all_words.append(elements[e].lower())
                    else:
                        all_words.append(word.lower())
    
    
    #Excluding punctuation from words
    puncts = "?:()!.,-;’'%”“`|{}<>=&"
    for word in all_words:
        words_no_punct.append(word.translate(str.maketrans('','', puncts)))
    words_no_punct = list(filter(None, words_no_punct))

    text = ' '.join(words_no_punct)

    #Excluding english stopwords
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(text)
    final_text = [word for word in word_tokens if not word in stop_words]
    return Counter(final_text).most_common(10)

def collect_authors(records):
    """Returns set of unique authors values"""

    authors = set()
    for r in records:
        authors.add(r[1])

    return authors

def generate_author_name(authors):
    """Generation strings of authors for service"""
    names = [(''.join(name.lower().split()), name) for name in authors]
    names_corrected = []

    for name in names:
        if 'ł' in name[0]:
            names_corrected.append((name[0].replace('ł', 'l'), name[1]))
        elif 'ą' in name[0]:
            names_corrected.append((name[0].replace('ą', 'a'), name[1]))
        else:
            names_corrected.append((name[0], name[1]))
    return names_corrected

if __name__ == '__main__':
    website = 'https://teonite.com/blog'
    
    conn = psycopg2.connect(host = 'db',
                            database = 'postgres',
                            user = 'postgres',
                            port = '5432')
    cur = conn.cursor()
    cur.execute("""
                CREATE TABLE IF NOT EXISTS teonite
                (
                id integer PRIMARY KEY NOT NULL,
                author text NOT NULL,
                post text NOT NULL
                )
                """)
    
    data = get_data(website)

    for d in data:
        cur.execute("""INSERT INTO teonite (id, author, post) 
                    VALUES (%s, %s, %s)""", (d[0], d[1], d[2]))
        conn.commit()

    cur.execute('SELECT * FROM teonite')
    records = cur.fetchall()

    frequent_words = extract_words(records)
    
    cur.execute("""
                CREATE TABLE IF NOT EXISTS stats
                (
                id SERIAL PRIMARY KEY,
                word text NOT NULL,
                counter integer NOT NULL
                )
                """)

    cur.execute('CREATE SEQUENCE stats_sequence start 1 increment 1')

    for word in frequent_words:
        cur.execute("""INSERT INTO stats (id, word, counter) 
                    VALUES (nextval('stats_sequence'), %s, %s)
                    """, (word[0], word[1]))
        conn.commit()
    
    authors = collect_authors(records)

    for author in authors:
        cur.execute("SELECT * FROM teonite WHERE author = '{}'".format(author))
        records_per_author = cur.fetchall()
        frequent_words_author = extract_words(records_per_author)
        table_name = 'stats_{}_{}'.format(author.split()[0].lower(), author.split()[1].lower())
        
        cur.execute('DROP SEQUENCE stats_sequence')

        cur.execute("""CREATE TABLE IF NOT EXISTS {} 
                    (
                    id SERIAL PRIMARY KEY, 
                    word text NOT NULL, 
                    counter integer NOT NULL
                    )""".format(table_name))

        cur.execute('CREATE SEQUENCE stats_sequence start 1 increment 1')

        for word in frequent_words_author:
            cur.execute("""INSERT INTO {} (id, word, counter) 
                        VALUES (nextval('stats_sequence'), '{}', {})
                        """.format(table_name, word[0], word[1]))
            conn.commit()

    names = generate_author_name(authors)

    cur.execute("""CREATE TABLE IF NOT EXISTS authors
                (
                id SERIAL PRIMARY KEY,
                address text NOT NULL,
                name text NOT NULL
                )"""
                )
    
    cur.execute('DROP SEQUENCE stats_sequence')
    
    cur.execute('CREATE SEQUENCE stats_sequence start 1 increment 1')
    
    for name in names:
        cur.execute("""INSERT INTO authors (id, address, name) 
                    VALUES (nextval('stats_sequence'), '{}', '{}')
                    """.format(name[0], name[1]))
        conn.commit()

    cur.close()
    conn.close()
