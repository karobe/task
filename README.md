# Task

Application for web scraping and displaying generated statistics 
in rest service.

## Assumptions

All of the data possessed from web scraping were saved to database.
Only english articles from website were taken into consideration with
removal of english stopwords.
Statistics generated for each address of rest service were recorded 
in particular tables within the same database and used to generate 
models for django rest framework API.

## Instructions

Use docker-compose up to run application.

```bash
docker-compose up
```

After establishing connection run the service and python script.

```bash
docker-compose run web python app.py
```
